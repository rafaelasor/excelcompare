var XLSX = require('xlsx');
const assert = require('assert');
var deepEqual = require('deep-equal')

if (process.argv.length < 4) {
	console.log("Invalid number of args");
	return;
}

let source = process.argv[2];
let target = process.argv[3];
let current = XLSX.readFile(source);
let baseline = XLSX.readFile(target);

var browser = this;

if (current && baseline) {

	errors = [];
	current.SheetNames.forEach(function (y) { /* iterate through sheets */
		var currentSheet = current.Sheets[y];
		var baselineSheet = baseline.Sheets[y];
		var lineTitle;

		for (var z in currentSheet) {
			/* all keys that do not begin with "!" correspond to cell addresses */
			if (z[0] === '!') {
				continue;
			}
			let currentValue = currentSheet[z].v;
			let baselineValue = baselineSheet[z] && baselineSheet[z].v;

			if(currentValue && currentValue.indexOf && currentValue.indexOf("Generated") == -1 && !deepEqual(currentValue, baselineValue)){
				errors.push({Sheet: y,  Cell: z,  Current: currentValue, Baseline: baselineValue});
			}
		}
	});

	for(var i =0; i < errors.length; i++){
		console.log(errors[i]);
	}

	if (errors.length == 0){
		console.log("File are the same");
	}
	console.log("Done");
};